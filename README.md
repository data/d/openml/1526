# OpenML dataset: wall-robot-navigation

https://www.openml.org/d/1526

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ananda Freire, Marcus Veloso and Guilherme Barreto     
**Source**: [original](http://www.openml.org/d/1497) - UCI     
**Please cite**:   

* Dataset Title: 

Wall-Following Robot Navigation Data Data Set (version with 4 Attributes)

* Abstract:  

The data were collected as the SCITOS G5 robot navigates through the room following the wall in a clockwise direction, for 4 rounds, using 24 ultrasound sensors arranged circularly around its 'waist'.

* Source:

(a) Creators: Ananda Freire, Marcus Veloso and Guilherme Barreto 
Department of Teleinformatics Engineering 
Federal University of CearÃ¡ 
Fortaleza, CearÃ¡, Brazil 

(b) Donors of database: Ananda Freire (anandalf '@' gmail.com) 
Guilherme Barreto (guilherme '@' deti.ufc.br)

* Data Set Information:

The provided file contain the raw values of the measurements of all 24 ultrasound sensors and the corresponding class label. Sensor readings are sampled at a rate of 9 samples per second. 

It is worth mentioning that the 24 ultrasound readings and the simplified distances were collected at the same time step, so each file has the same number of rows (one for each sampling time step). 

The wall-following task and data gathering were designed to test the hypothesis that this apparently simple navigation task is indeed a non-linearly separable classification task. Thus, linear classifiers, such as the Perceptron network, are not able to learn the task and command the robot around the room without collisions. Nonlinear neural classifiers, such as the MLP network, are able to learn the task and command the robot successfully without collisions. 

If some kind of short-term memory mechanism is provided to the neural classifiers, their performances are improved in general. For example, if past inputs are provided together with current sensor readings, even the Perceptron becomes able to learn the task and command the robot successfully. If a recurrent neural network, such as the Elman network, is used to learn the task, the resulting dynamical classifier is able to learn the task using less hidden neurons than the MLP network. 

* Attribute Information:

Number of Attributes: sensor_readings_24.data: 24 numeric attributes and the class. 

For Each Attribute: 
-- File sensor_readings_4.data: 
1. SD_front: minimum sensor reading within a 60 degree arc located at the front of the robot - (numeric: real) 
2. SD_left: minimum sensor reading within a 60 degree arc located at the left of the robot - (numeric: real) 
3. SD_right: minimum sensor reading within a 60 degree arc located at the right of the robot - (numeric: real) 
4. SD_back: minimum sensor reading within a 60 degree arc located at the back of the robot - (numeric: real) 
5. Class: {Move-Forward, Slight-Right-Turn, Sharp-Right-Turn, Slight-Left-Turn}   


* Relevant Papers:

Ananda L. Freire, Guilherme A. Barreto, Marcus Veloso and Antonio T. Varela (2009), 
'Short-Term Memory Mechanisms in Neural Network Learning of Robot Navigation 
Tasks: A Case Study'. Proceedings of the 6th Latin American Robotics Symposium (LARS'2009), 
ValparaÃ­so-Chile, pages 1-6, DOI: 10.1109/LARS.2009.5418323

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1526) of an [OpenML dataset](https://www.openml.org/d/1526). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1526/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1526/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1526/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

